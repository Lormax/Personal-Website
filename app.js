gsap.registerPlugin(ScrollTrigger);

const titreh1 = document.querySelector('.landing-page h1');
const titreh2 = document.querySelector('.landing-page h2');
const titrebtn = document.querySelector('.landing-page button');
const nav = document.querySelector('.nav.nav');
const nava = document.querySelectorAll('.nav a');
const logo = document.querySelector('.nav img');
const img = document.querySelector(".about img");
const about = document.querySelector(".about-text ")


/* Transparance au scroll Bar navigation */
window.addEventListener('scroll',() => {
if(window.scrollY > 30){
    nava.forEach(element => {
        element.classList.add('scroll');
    });
    nav.classList.add('scroll');
    logo.setAttribute("src","img/logo.png")
}else{
    
    nava.forEach(element => {
        element.classList.remove('scroll');
    });
    nav.classList.remove('scroll');
    logo.setAttribute("src","img/logoBlanc.png")
}
})

/* Animation  Titre landing page */

window.addEventListener('load', () => {


    var tl = gsap.timeline({paused: true});

    tl.staggerFrom(titreh1, 1, {x: -100, opacity:0, ease:"power2.out"},0.3);
    tl.staggerFrom(titreh2, 1, {x: -100, opacity:0, ease:"power2.out"},0.3);
    tl.staggerFrom(titrebtn, 1, {x: -100, opacity:0, ease:"power2.out"},0.3);
    
    tl.play();
});


/*Animation About */


gsap.from(img,1,{scrollTrigger:".about button", transform:"scale(0)", opacity:0, ease:"power2.out"});
gsap.from(about,1,{scrollTrigger:".about button", x:1000, opacity:0, ease:"power2.out"});